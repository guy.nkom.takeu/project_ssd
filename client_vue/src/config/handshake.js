import axios  from "axios";
import { pki, util } from "node-forge";
import { HTTP } from "../api/api.js"


export default{
    handshake: async function(){
        let keypair;
        await pki.generateKeyPair({ bits: 2048, workers: 2 }, (err, k) => {
            keypair = k;
        });

        //converting public key to PEM format
        const publicKey = pki.publicKeyToPem(keypair.publicKey);

        //sending the public key to server and save the encyrpted shared key
        const { encryptedKey, handshakeId } = await HTTP.
        post('/handshake',{
            publicKey
        }).then((res) => res.data)
        .catch((error) => error);
        return {encryptedKey, handshakeId };
    }
}