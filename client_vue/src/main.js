import { createApp } from 'vue'
import { createPinia } from 'pinia'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BaseLayout from '../src/components/BaseLayout.vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
// import VueRouter from 'vue-router'

import App from './App.vue'
import router from './router'

import './styles.css'
import './index.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.component("base-layout", BaseLayout);

app.mount('#app')

//  const routes = new VueRouter({
//     router
// })

router.beforeEach((to, from, next) => {
     if (to.path == '/login') {
    sessionStorage.removeItem('token');
  }
  let authenticationresponse = JSON.parse(sessionStorage.getItem('token'));
  if (!authenticationresponse && to.path != '/login') {
    next({ path: '/login' });
  } else {
    next();
  }
 //next();
})
