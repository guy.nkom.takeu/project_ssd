import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '../views/DashboardView.vue'
import LoginPage from '../views/LoginPage.vue'
import EventPage from '../views/EventPage.vue'
import AddEvent from '../views/AddEvent.vue'
import AddContact from '../views/AddContact.vue'
import ContactPage from '../views/ContactPage.vue'
import SignupPage from '../views/SignupPage.vue'
import TestView from '../views/TestView.vue'
import ViewEvent from '../views/ViewEvent.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardView
    },
   
    {
      path: '/test',
      name: 'test',
      component: TestView
    },
    {
      path: '/detail',
      name: 'detail',
      component: ViewEvent
    },
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   component: () => import('../views/DashboardView.vue')
    // },
    {
      path: '/login',
      name: 'login',
      component: LoginPage
    },
    {
      path: '/event',
      name: 'event',
      component: EventPage
    },
    {
      path: '/addevent',
      name: 'addevent',
      component: AddEvent
    },
    {
      path: '/addcontact',
      name: 'addcontact',
      component: AddContact
    },
    {
      path: '/contact',
      name: 'contact',
      component: ContactPage
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignupPage
    },
  ]
})

export default router
