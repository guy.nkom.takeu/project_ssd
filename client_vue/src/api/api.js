import axios from "axios"
import util from "../api/util.js"

export const BASE_URL = `http://localhost:8000/agenda/`;
export const HTTP = axios.create({
    baseURL: BASE_URL,
    validateStatus: false,
    headers: {
       accept : 'application/json',
        
    },
});

export const LOGIN_REQUEST = params => { return HTTP.post(`login`,params).then(res => res.data);};
export const POST_UTILISATEUR = params => {return HTTP.post(`signup`, params).then(res => res.data);};
export const POST_EVENT = params => { return HTTP.post(`event/add`, params,util.getToken()).then(res => res.data);};
export const POST_TASK = params => { return HTTP.post(`task/add`, params).then(res => res.data);};
export const POST_KEY = params => { return HTTP.post(`handshake`, params).then(res => res.data);};