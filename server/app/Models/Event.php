<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = [
        'name','location','start_date','end_date','description','start_time','end_time','author'
    ];

    public function members(){
        return $this->hasMany(User::class);
    }

    public function author(){
        return $this->belongsTo(User::class);
    }
}
