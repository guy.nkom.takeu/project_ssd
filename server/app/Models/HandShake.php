<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HandShake extends Model
{
    protected $fillable = [
        'shared_key','expires_at'
    ];
    protected $dates = ['expires_at'];
}
