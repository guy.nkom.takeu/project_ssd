<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'groupe','description','reference','quantity','price'
    ];

    //cardinalite
    public function category() {
        return $this->belongTo(Category::class);
    }


}

