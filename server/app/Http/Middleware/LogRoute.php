<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        if (app()->environment('local')){
            $data = [
                'request' => $request->all(),
                'response' => $response->getContent()
            ];
            Log::info(json_encode($data));
        }

        return $response;
    }
}
