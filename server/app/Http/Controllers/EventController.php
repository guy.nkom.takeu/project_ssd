<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\HandShake;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        if ($events->count() <= 0){
            return response()->json('No Events available ', 401);
        } else {
            return response()->json($events, 200);

        }
    }


    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'name' => ['required', 'max:15']
        // ]);

        $handshakeId = $request->get('id');
        //Log::channel('stderr')->info($handshakeId);
        $validHandshake = HandShake::whereId($handshakeId)
        ->whereRaw(
            "expires_at > STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')",
            Carbon::now()->format('Y-m-d H:i:s')
        )
        ->first();

        if(!$validHandshake)
        return response()->json(['error' => 'invalid handshake'], 422);
        Log::channel('stderr')->info("encrypted name :" . $request->get('name'));
        $encryptedMsg = base64_decode($request->get('name'));
        $iv = mb_substr($encryptedMsg,0, 16, '8bit');
        $encrypted = mb_substr($encryptedMsg, 16, null, '8bit');

        $decryptedData = openssl_decrypt(
            $encrypted,
            'aes-256-cbc',
            $validHandshake->shared_key,
            OPENSSL_RAW_DATA,
            $iv
        );


        $event = new Event();
        $event->name = $decryptedData;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->start_time = $request->start_time;
        $event->end_time = $request->end_time;
        $event->author = auth()->user()->name;
        $event->location = $request->location;
        //$event->fill($request->all());
        $event->save();

        return response()->json('success',200);
    }

    public function display($author){
        //$event = Category::where('label', '=', $word)->get();
        $events = Event::where('author', '=', $author)->get();
        return response()->json($events, 200);
    }

    public function modify(Request $request, $id) {

        $event = Event::findOrFail($id);
        $this->validate($request,[
            'name' => ['required', 'max:15']
        ]);
        $event = new Event();
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_data;
        $event->start_time = $request->start_time;
        $event->end_time = $request->end_time;
        $event->author = auth()->user()->name;
        $event->location = $request->location;
        $event->fill($request->all());
        $event->save();
        return response()->json('success',200);
    }

    public function delete($id) {
        $event = Event::findOrFail($id);
        if($event->author != auth()->user()->name){
            return response()->json('You are not the owner', 401);
        }
        $event->delete();

        return response()->json('delete',200);
    }

    public function sentEvent(Request $request){
        $email = $request->get('email');
    }




}
