<?php

namespace App\Http\Controllers;

use App\Models\HandShake;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TaskController extends Controller
{
    public function index(){
        $task = Task::all();
        return response()->json($task, 200);
    }

    public function add(Request $request){

        $handshakeId = $request->get('id');
        //Log::channel('stderr')->info($handshakeId);
        $validHandshake = HandShake::whereId($handshakeId)
        ->whereRaw(
            "expires_at > STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')",
            Carbon::now()->format('Y-m-d H:i:s')
        )
        ->first();

        if(!$validHandshake)
        return response()->json(['error' => 'invalid handshake'], 422);
        $encryptedMsg = base64_decode($request->get('name'));
        $iv = mb_substr($encryptedMsg,0, 16, '8bit');
        $encrypted = mb_substr($encryptedMsg, 16, null, '8bit');
        $decryptedData = openssl_decrypt(
            $encrypted,
            'aes-256-cbc',
            $validHandshake->shared_key,
            OPENSSL_RAW_DATA,
            $iv
        );

        $task = new Task();
        $task->name = $decryptedData;
        $task->save();
    }
}
