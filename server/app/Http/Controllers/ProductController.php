<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function display(){
        $products = Product::all()->order_by('name');
        return response()->json($products,200);
    }

    public function add(Request $request){
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->category_id = $request->category_id;
        $product->type_id = $request->type_id;
        $product->image = $request->image;
        $product->save();
        if($product){
            return response()->json('product added with success',200);
        } else {
            return response()->json('Error add',401);
        }

    }

    public function modify(Request $request, $id){
        $this->validate($request, [
            'groupe' => ['required', 'max:20'],
            'price' => 'required',
            'description' => 'required',
            'category_id' => 'required'
        ]);

        $product = new Product();
        $product = Product::findOrFail($id);
        $product-> quantity = $request->quantity;
        $product->type_id = $request->type_id;
        $product->fill($request->all());
        $product->save();
        if($product){
            return response()->json('product upgrated with success',200);
        } else {
            return response()->json('Error upgrate',401);
        }

    }

    public function search($word) {
        $product = new Product();
        $products = $product->category()->label;

    }
}
