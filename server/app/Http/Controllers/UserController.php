<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return response()->json($users, 200);
    }

    public function displayMed(){
        $users = DB::select('select u.name, u.immat, u.phone, u.email, u.sex from users u, roles r where u.role_id = r.id and r.name = "MED"');
        return response()->json($users, 200);
    }

    public function addContact(Request $request){
        $mail = $request->get('email');
        $user = User::where('email','=',$mail)->get(); //finding the user to add
        $user->sender = auth()->user()->name;
        $user->status = "pending";
        $user->replicate()->setTable('contacts')->save();

    }


}
