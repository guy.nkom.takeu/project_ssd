<?php

namespace App\Http\Controllers;

use App\Models\HandShake;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function __construct(){
        //exception of use case whose don't need token to function
        return $this-> middleware('auth:api', ['except' => ['login','signup']]);
    }

    public function signup(Request $request){
        $this->validate($request,[
            'name' => ['required'],
            'email' => ['required','email'],

        ]);


        //create instance (object)
        $user = new User();
        $user->address = $request->address;
        $user->fname = $request->fname;
        $user->phone = $request->phone;
        $user->image = $request->image;
        $user->role_id = $request->role_id;
        $user->password = Hash::make($request->password);
        $user->fill($request->all());
        $user->save();
        if($user){
            return response()->json('user added with success',200);
        } else {
            return response()->json('Error add',401);
        }
    }


    public function login(Request $request){
        $this->validate($request, [
            'email',
            'password'
        ]);


        $email = $request->email;
        $password = $request->password;
        //$password = $decryptedData;

        $credentials = request(['email','password']);
        if(!$token = auth()->attempt($credentials)){
            return response()->json(['error' =>'unautorize'],401);
        }

        return $this->generateToken($token);
    }

    protected function generateToken($token){
        return response()->json([
        'user_role' => auth()->user()->role->name,
        'user' => auth()->user(),
        'access_token'=> $token,
        'token_type'=>'bearer',
        'expired'=>auth()->factory()->getTTL()*600]);
    }

    public function logOut() {
        auth()->logOut();
        return response()->json('log out successfuly');
    }

    public function currentUser() {
        auth()->user();
        return auth()->user()->name;
    }

    public function refreshToken() {
        return $this->generateToken(auth()->refresh());
    }
}
