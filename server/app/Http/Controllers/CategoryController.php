<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function display()
    {
        $categories = Category::all();
        if ($categories->count() <= 0){
            return response()->json('pas de categories disponible', 401);
        } else {
            return response()->json($categories, 200);

        }
    }

    public function add(Request $request) {
        $this->validate($request,[
            'label' => ['required', 'max:15']
        ]);
        $category = new Category();
        $category -> description = $request ->description;
        //partie conditionnee
        $category->fill($request->all());
        $category->save();

        return response()->json('success',200);
    }

    public function modify(Request $request, $id) {
        $category = Category::findOrFail($id);
        $category-> label = $request -> label;
        $category-> description = $request -> description;
        $category->save();

        return response()->json('success',200);
    }

    public function details($id) {
        $category = Category::where('id', '=', $id)->get();

        return response()->json($category, 200);
    }

    public function delete($id) {
        $category = Category::findOrFail($id);
        $category-> delete();

        return response()->json('delete',200);
    }

    public function search($word){
        $category = Category::where('label', '=', $word)->get();
        if($category->count()<= 0){
            return response()->json('no category',401);
        } else {
            return response()->json($category,200);
        }
    }

}