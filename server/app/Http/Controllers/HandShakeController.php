<?php

namespace App\Http\Controllers;

use App\Models\HandShake;
use Carbon\Carbon;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HandShakeController extends Controller
{
    public function handshake(Request $request){

        $publicKey = $request->get('publicKey');
        //Log::channel('stderr')->info($publicKey);
        //$trimKey = str_replace(" ","+", $publicKey);

        //generating random key to be used as shared key
        $data = Str::random(32);
        openssl_public_encrypt($data, $encrypted_data, $publicKey, OPENSSL_PKCS1_PADDING);

        //saving the keep temporarily
        $handshake = HandShake::create(
            [
                'shared_key' => $data,
                'expires_at' => Carbon::now()->addMinutes(5),
            ]
        );

        //return encrypted sharedkey and handshake id
        return response()->json(
            [
                'encryptedKey' => base64_encode($encrypted_data),
                'handshakeId' => $handshake->id,
            ]
        );
    }
}
