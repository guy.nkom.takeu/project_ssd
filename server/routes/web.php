<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Welcome to our agenda backend";
});

$router->group(['prefix' => 'agenda'], function() use ($router) {
    $router->post('login', ['uses' => 'AuthController@login']);
    $router->post('signup', ['uses' => 'AuthController@signup']);
    $router->post('product', ['uses' => 'ProductController@add']);
    $router->get('meds', ['uses' => 'UserController@displayMed']);
    $router->post('role/add', ['uses' => 'RoleController@index']);
    $router->post('task/add', ['uses' => 'TaskController@add']);
    $router->get('task/all', ['uses' => 'TaskController@index']);
    $router->post('/handshake', ['uses' => 'HandShakeController@handshake']);

});

$router->group(['prefix' => 'agenda','middleware' => 'auth'], function() use ($router) {
    $router->get('events/all', ['uses' => 'EventController@index']);
    $router->post('event/add', ['uses' => 'EventController@store']);
    $router->post('user/contact', ['uses' => 'UserController@addContact']);
    $router->get('user/all', ['uses' => 'UserController@index']);
    $router->get('events/{author}', ['uses' => 'EventController@display']);
    $router->put('category/{id}', ['uses' => 'CategoryController@modify']);
    $router->delete('category/{id}', ['uses' => 'CategoryController@delete']);
    $router->get('category/{id}', ['uses' => 'CategoryController@details']);
    $router->get('categories/{word}', ['uses' => 'CategoryController@search']);
    $router->post('me', ['uses' => 'AuthController@currentUser']);
    $router->post('logout', ['uses' => 'AuthController@logOut']);
    $router->put('eventt/{author}', ['uses' => 'EventController@modify']);
    $router->delete('event/{id}', ['uses' => 'EventController@delete']);

    });
