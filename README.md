## project_ssd

#Members of the group(Names and matricule)

	NKOM TAKEU GUY JUNIOR :   000562542
	WANDJA GHISLAIN GEOVANI : 000507001

##project name : agenda

#Requirements 

	- MySQL
	- Node js
	- NPM
	- Vue js
	- PHP version >= 8.0
	- OpenSSL PHP Extension
	- PDO PHP Extension
	- Mbstring PHP Extension
	- Composer

#Server installation guide
	
	- Create a MySQL database locally named "ssd_project" utf8_general_ci 
	- Download Composer,  https://getcomposer.org/download/ 
	- Pull Project_ssd project from the git provider
	- Get to your MySQL DBMS and import the sql file named "ssd_project.sql" found in the root directory to your database
	- Open the console and cd the directory named "server"
	- Rename the .env.example file to .env inside the server directory and fill in the information of your database (database username and password)
	- Run the following command to install dependencies : composer install
	- Run the following command to create your app key : php artisan key:generate
	- Run the following command to launch the server : php -S localhost:8000 -t public 
	- You can chose any available port at your convenience

#Client installation guide

	- After downloading node js and npm to your system
	- Pull Project_ssd project from the git provider 
	- Open the console and cd the directory named "client_vue"
	- Run the following command to insall dependencies : npm install 
	- Run the following command to launch the client : npm run dev
	- Use the URL proposed to you to get access to the application on a browser

#NB
	- Launch the server before launching the client
	- Chose only available port numbers to launch the server


	
	


