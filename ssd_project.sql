-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ssd_project
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (9,'Reading','Readint the news of today','guy','2023-01-21','2023-01-15','21:38:00','21:39:00','Louvain','2023-01-08 20:34:41','2023-01-08 20:34:41'),(10,'Playing the pianor','Playing with the kids','guy','2023-01-11','2023-01-22','21:35:00','23:40:00','Namur','2023-01-08 20:35:15','2023-01-08 20:35:15'),(11,'Sending mails','Sending emails to the senders','guy','2023-01-13','2023-01-22','21:35:00','23:40:00','Maxville','2023-01-08 20:35:56','2023-01-08 20:35:56'),(12,'Receiving gifts','Receiving gifts for the poor','marie','2023-01-09','2023-01-15','21:38:00','21:40:00','Paris','2023-01-08 20:36:53','2023-01-08 20:36:53'),(13,'Teachiing in HEB','Teaching with the students','marie','2023-01-11','2023-01-22','21:38:00','23:44:00','Park','2023-01-08 20:38:17','2023-01-08 20:38:17'),(14,'Goind to church','Going to church everyday','barclay','2023-01-08','2023-01-14','21:41:00','22:42:00','Bafoussam','2023-01-08 20:40:04','2023-01-08 20:40:04');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hand_shakes`
--

DROP TABLE IF EXISTS `hand_shakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hand_shakes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `shared_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hand_shakes`
--

LOCK TABLES `hand_shakes` WRITE;
/*!40000 ALTER TABLE `hand_shakes` DISABLE KEYS */;
INSERT INTO `hand_shakes` VALUES (1,'cqqAJy1erShHKKc9laKw74qfn8KlgQMQ','2023-01-08 15:28:07','2023-01-08 15:23:07','2023-01-08 15:23:07'),(2,'gM3t2vx6TBuFjLmesEkTV93QddRci6gs','2023-01-08 15:30:19','2023-01-08 15:25:19','2023-01-08 15:25:19'),(3,'oQVomHaj8twkcnOmBqBXVMHyPrTrxVYd','2023-01-08 15:39:55','2023-01-08 15:34:55','2023-01-08 15:34:55'),(4,'8dlDqlwOtMFvsYNolZ7iH7U3Zf5zrRIS','2023-01-08 15:41:22','2023-01-08 15:36:22','2023-01-08 15:36:22'),(5,'HL3PcjFxydlZ0W5mugYAUqow5F2KdQb9','2023-01-08 15:41:56','2023-01-08 15:36:56','2023-01-08 15:36:56'),(6,'FLbDGrqQmq1jU7ldlSgC0aeM98NhlA2Y','2023-01-08 15:42:05','2023-01-08 15:37:05','2023-01-08 15:37:05'),(7,'DX5pdOXEQS41tyGvJh4ilmeOmYGA5rO7','2023-01-08 15:42:21','2023-01-08 15:37:21','2023-01-08 15:37:21'),(8,'6fbDqSWV4bdvHpicQRByDM4ZcpbMpu07','2023-01-08 15:43:00','2023-01-08 15:38:00','2023-01-08 15:38:00'),(9,'OZrQJrdoT2I5jGh76jd3Og3BdyCjK4SE','2023-01-08 15:43:47','2023-01-08 15:38:47','2023-01-08 15:38:47'),(10,'JYTiNXOHDHNbhwo1Ezi51IlJDOcdotmY','2023-01-08 15:45:55','2023-01-08 15:40:55','2023-01-08 15:40:55'),(11,'xLBuzfP6mHDpMjtzLLcKUw5kctpsowNF','2023-01-08 15:46:00','2023-01-08 15:41:00','2023-01-08 15:41:00'),(12,'cWkt4B2eHncIw4xw40vRpUWF4OXi2bvT','2023-01-08 15:46:11','2023-01-08 15:41:11','2023-01-08 15:41:11'),(13,'1UIETrJ1QrC0pSHnMBzLsjecpMVZCjb7','2023-01-08 15:46:56','2023-01-08 15:41:56','2023-01-08 15:41:56'),(14,'bL46oRyGpvl4WWx0tocV3VLhjtjk97np','2023-01-08 15:47:07','2023-01-08 15:42:07','2023-01-08 15:42:07'),(15,'7Hm7TumXYFiepatPACd1gK7II9W3ZbFM','2023-01-08 15:48:02','2023-01-08 15:43:02','2023-01-08 15:43:02'),(16,'DpXM3ygoKwhm67TJyEIoedbgL31mI8I0','2023-01-08 15:48:29','2023-01-08 15:43:29','2023-01-08 15:43:29'),(17,'zsGmEruSl0QKAIheTZmpwqf90KBogfBF','2023-01-08 15:48:49','2023-01-08 15:43:49','2023-01-08 15:43:49'),(18,'h0CGYyOLhD0tRxDcdhzJApzYbnQnlIrp','2023-01-08 15:49:57','2023-01-08 15:44:57','2023-01-08 15:44:57'),(19,'79NF8sNcc1oFLyyCzjA5Cs50BkM5YEHo','2023-01-08 15:50:08','2023-01-08 15:45:08','2023-01-08 15:45:08'),(20,'FFldWwO9PX1Sp3r9Gt6d8bYsSp5mc7q8','2023-01-08 15:50:18','2023-01-08 15:45:18','2023-01-08 15:45:18'),(21,'Zk405IEjwfoMm2spYCzrG7FHb64i2v9F','2023-01-08 15:54:59','2023-01-08 15:49:59','2023-01-08 15:49:59'),(22,'S3pJiyEjJUvVKbTzZPaAfJEtJSsF7IcX','2023-01-08 15:55:30','2023-01-08 15:50:30','2023-01-08 15:50:30'),(23,'3DnvhwmVbHEvSGZdrtgVb6J7euioXhr2','2023-01-08 15:56:09','2023-01-08 15:51:09','2023-01-08 15:51:09'),(24,'OorFKWGdXdQfoA5LEUitVB6Lw4EatVPt','2023-01-08 15:57:23','2023-01-08 15:52:23','2023-01-08 15:52:23'),(25,'RgCTLTrxPiJe1uyRkNGnW3brR5WYCtxE','2023-01-08 15:58:18','2023-01-08 15:53:18','2023-01-08 15:53:18'),(26,'2RWLZf0gmEPJRbFPlWVlbtSG0ME6vzyz','2023-01-08 16:02:06','2023-01-08 15:57:06','2023-01-08 15:57:06'),(27,'qU1g3NgtQ9YUEhGb0rqeb9SX9GFvvUNY','2023-01-08 16:02:35','2023-01-08 15:57:35','2023-01-08 15:57:35'),(28,'iLXPOtsWhbMKmaiQNoFGgDrhTsnoZ9jG','2023-01-08 16:03:44','2023-01-08 15:58:44','2023-01-08 15:58:44'),(29,'8Lw98s0jugIHVB5gWctcwyEA8CuZNSQy','2023-01-08 16:03:58','2023-01-08 15:58:58','2023-01-08 15:58:58'),(30,'vlkE2tsXmszwWXZoo5iJdXukYB9vZmPm','2023-01-08 16:06:11','2023-01-08 16:01:11','2023-01-08 16:01:11'),(31,'4rdjt7lNLZ6X9gE03aRLwnl5n5dADaDW','2023-01-08 16:06:27','2023-01-08 16:01:27','2023-01-08 16:01:27'),(32,'QK9UPJMHdBU2fkg3IQsAW7DVSrEipEyE','2023-01-08 16:07:20','2023-01-08 16:02:20','2023-01-08 16:02:20'),(33,'Xs2ToQWJl537ZPBH7x4bhVJQYGyWUWQZ','2023-01-08 16:07:37','2023-01-08 16:02:37','2023-01-08 16:02:37'),(34,'30LB7SRdRTQjPV4hKHq6k1G0lTHT57sE','2023-01-08 16:09:08','2023-01-08 16:04:08','2023-01-08 16:04:08'),(35,'ZV35VQcVJsKTeewU7SvufYcwMTMAuC4W','2023-01-08 16:09:14','2023-01-08 16:04:14','2023-01-08 16:04:14'),(36,'FSBHO9GH0QXVlUME5LrXh6stQmqibgEz','2023-01-08 16:09:34','2023-01-08 16:04:34','2023-01-08 16:04:34'),(37,'z0XicOImTog2r2a7uV6XohjIwQ33zTsz','2023-01-08 16:09:38','2023-01-08 16:04:38','2023-01-08 16:04:38'),(38,'AJNAUcvcbgFzBHF8HtAM09yeFOvRFoBL','2023-01-08 16:09:58','2023-01-08 16:04:58','2023-01-08 16:04:58'),(39,'GUaNLUNdLJQkudg9UzjpHYIwOMLav8Qh','2023-01-08 16:11:45','2023-01-08 16:06:45','2023-01-08 16:06:45'),(40,'yOy2e1yVeG4nzQtH26bSiekiyzsj6fjf','2023-01-08 16:12:53','2023-01-08 16:07:53','2023-01-08 16:07:53'),(41,'WjEcSkd5QlBLokbX8lgZ9GQICFUVotlb','2023-01-08 16:20:38','2023-01-08 16:15:38','2023-01-08 16:15:38'),(42,'XkVX6ajEFKqIfFT6oo5PAf55bEIVM4Wq','2023-01-08 16:21:07','2023-01-08 16:16:07','2023-01-08 16:16:07'),(43,'WlQDIFy0nNxc0dDKk8WQEvkDhtymDooU','2023-01-08 16:21:10','2023-01-08 16:16:10','2023-01-08 16:16:10'),(44,'yfZ13i4syJMuexl1DV3TS2vGjgfWkHBZ','2023-01-08 16:22:00','2023-01-08 16:17:00','2023-01-08 16:17:00'),(45,'HmpEGzJlbPnoStY7sxwSL7Tq97hm1lWi','2023-01-08 16:22:16','2023-01-08 16:17:16','2023-01-08 16:17:16'),(46,'bVkQeMV5JnJjRzdgCd5jRv7nJFNKA4cp','2023-01-08 16:23:13','2023-01-08 16:18:13','2023-01-08 16:18:13'),(47,'I6wtoVLS3D3WofmMdaXBuzqhLUQIooUb','2023-01-08 16:25:59','2023-01-08 16:21:00','2023-01-08 16:21:00'),(48,'ByUzxn7cbJHOqPk6iQ127XkUph6JuiAX','2023-01-08 16:26:43','2023-01-08 16:21:43','2023-01-08 16:21:43'),(49,'ahmazY5PRsDyyhp7wgBCaGy4Hr2LsLpq','2023-01-08 16:28:27','2023-01-08 16:23:27','2023-01-08 16:23:27'),(50,'vTKJvmnz7zk2awaXh2MYns4rWwoC1eBs','2023-01-08 16:31:15','2023-01-08 16:26:15','2023-01-08 16:26:15'),(51,'v2ieh6nSLjAtUmIf0BMZESXPPLGXr2xx','2023-01-08 16:32:41','2023-01-08 16:27:41','2023-01-08 16:27:41'),(52,'MA1htaiJRabXuoQue2w1H2NIWhYJyXpm','2023-01-08 16:39:13','2023-01-08 16:34:13','2023-01-08 16:34:13'),(53,'mSflzgqVWm1l7EVdK2YD5f4kMXxBtUYm','2023-01-08 17:16:09','2023-01-08 17:11:09','2023-01-08 17:11:09'),(54,'gJIDpnzAfCAmhULfeXWYZf1VjUoqTyA1','2023-01-08 17:20:03','2023-01-08 17:15:03','2023-01-08 17:15:03'),(55,'UAElB1HuuWkldzMySDWq8Wxiii8rZ1RW','2023-01-08 17:21:32','2023-01-08 17:16:32','2023-01-08 17:16:32'),(56,'sTm1AA79bQPDqHtASAGsKJb9wUCLqTF2','2023-01-08 17:22:38','2023-01-08 17:17:38','2023-01-08 17:17:38'),(57,'YQL7NFLEg1HOBSHXvw0jUmfrHRaMwp2w','2023-01-08 17:23:48','2023-01-08 17:18:48','2023-01-08 17:18:48'),(58,'3GERZapFNQZEQIcPRC51UQ5J1oVfmBP8','2023-01-08 17:24:21','2023-01-08 17:19:21','2023-01-08 17:19:21'),(59,'FHHUplDqsA0zjN7GwcgOGrkEzhnFoytr','2023-01-08 17:26:19','2023-01-08 17:21:19','2023-01-08 17:21:19'),(60,'v9Fg5HAfesATn6q3oJ6WCSXSZ3ZWMoop','2023-01-08 17:27:22','2023-01-08 17:22:22','2023-01-08 17:22:22'),(61,'we24MZmgianZNzJ16D0cmr7GrCiBq4dg','2023-01-08 17:48:49','2023-01-08 17:43:49','2023-01-08 17:43:49'),(62,'YPbsqxa2u55U4K50ePeuogAhvuHUwnN6','2023-01-08 17:51:34','2023-01-08 17:46:34','2023-01-08 17:46:34'),(63,'by93veLNJ0ptgbYHvc6tLkLvcIKLUmJb','2023-01-08 17:53:47','2023-01-08 17:48:47','2023-01-08 17:48:47'),(64,'zcACUB9sbPfI9uLRP8YgNg3veFVcyYGn','2023-01-08 17:56:23','2023-01-08 17:51:23','2023-01-08 17:51:23'),(65,'IKsnNOWGjEK7XhFEgcJPR264wdCrmVHi','2023-01-08 17:57:17','2023-01-08 17:52:17','2023-01-08 17:52:17'),(66,'F5jiuHcDO26eUg3szZJUmI0K8qlqWJPr','2023-01-08 18:01:39','2023-01-08 17:56:40','2023-01-08 17:56:40'),(67,'uSmCmjjwGELQTzBjvTgFthIzMGKawBWe','2023-01-08 18:03:30','2023-01-08 17:58:30','2023-01-08 17:58:30'),(68,'zLVgdw6TkefrWenIEochIv6gPMKjJHLQ','2023-01-08 18:06:43','2023-01-08 18:01:43','2023-01-08 18:01:43'),(69,'zcbjIFjy8weVQew5IV8VsshrxUMt20N3','2023-01-08 18:08:44','2023-01-08 18:03:44','2023-01-08 18:03:44'),(70,'TdBafkcNCpSGpNA7Fj5poqo8Y50w4cdY','2023-01-08 18:13:36','2023-01-08 18:08:36','2023-01-08 18:08:36'),(71,'PW6QVFLomseAVPAqrRz4DgKNIEhzAp5Y','2023-01-08 18:13:50','2023-01-08 18:08:50','2023-01-08 18:08:50'),(72,'yQnneEbXIoaAIGTS90r2kPFztJgcBotR','2023-01-08 18:15:47','2023-01-08 18:10:47','2023-01-08 18:10:47'),(73,'sqMv6ZF89RVIlHUth7AuNFjEhH0rCGO7','2023-01-08 18:16:49','2023-01-08 18:11:49','2023-01-08 18:11:49'),(74,'ni9TU1uexypyDEPc8gOpZlJoNcvnwFzz','2023-01-08 18:17:25','2023-01-08 18:12:25','2023-01-08 18:12:25'),(75,'YDu7d7QiGxOQDU34WEUhoN67zX6FZkbe','2023-01-08 18:17:54','2023-01-08 18:12:54','2023-01-08 18:12:54'),(76,'MaPbGCzdwnROKoT0oc8hCNZ5NfmS3V6S','2023-01-08 19:30:52','2023-01-08 19:25:52','2023-01-08 19:25:52'),(77,'tn1OGxSm9S4aTKkmQsfD6noiMxSnR6ao','2023-01-08 20:18:34','2023-01-08 20:13:34','2023-01-08 20:13:34'),(78,'HGp4CJSb9Rz2ZjpR65P4Xpf5p5UrR4H5','2023-01-08 20:30:07','2023-01-08 20:25:07','2023-01-08 20:25:07'),(79,'z6eCE0uxaGJ39yUMlc77COkdprP9Is3J','2023-01-08 20:37:59','2023-01-08 20:32:59','2023-01-08 20:32:59'),(80,'1EWIlfofmGObPRe7xF28X7WeRhhR2mqx','2023-01-08 20:39:40','2023-01-08 20:34:40','2023-01-08 20:34:40'),(81,'xEA4SddE3QTyWJWNPybke5YAAl6ghwgc','2023-01-08 20:40:15','2023-01-08 20:35:15','2023-01-08 20:35:15'),(82,'coqueJe5HHTVJTIRAJHlEh9vL6RGUrDD','2023-01-08 20:40:56','2023-01-08 20:35:56','2023-01-08 20:35:56'),(83,'GDBXcw6ZWt0jrvbhEKtwZTLNFYt7PepP','2023-01-08 20:41:53','2023-01-08 20:36:53','2023-01-08 20:36:53'),(84,'Nu6FIO2Gpn0gKjffv0kasewrjv9gWGhx','2023-01-08 20:43:17','2023-01-08 20:38:17','2023-01-08 20:38:17'),(85,'lxkf5vt0QXDUfmCsiMgPqsR25O3KY1q3','2023-01-08 20:45:04','2023-01-08 20:40:04','2023-01-08 20:40:04');
/*!40000 ALTER TABLE `hand_shakes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (26,'2022_10_09_172737_create_users_table',1),(27,'2022_10_19_223549_create_roles_table',1),(28,'2022_12_28_171251_create_events_table',1),(29,'2023_01_06_093016_create_contacts_table',1),(30,'2023_01_06_114440_create_tasks_table',1),(31,'2023_01_06_123459_create_hand_shakes_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tasks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'study'),(2,'Bwbp1YWA6ZMmYhZ/KdVY/0ejNeuokMMZU6o63hBqD6E='),(3,'tsIz9F2GEtzOOPTNz6z/oKR5RBL2kB1M4UAywdPqyZw='),(4,'M7I8cWV7e6klX4zYVIyycXG2uKmjLULC7TY46C5ANdg='),(5,'marie'),(6,'marco polo');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'guy','junior','tours','guy@gmail.com','06894323','$2y$10$hgpgn5jxN/bjbsB1C.ckHeLgKaqjRsX48aBSueJprkSRiGQeNT0Re','guyjr.img',NULL,NULL,1,'2023-01-08 12:45:58','2023-01-08 12:45:58'),(5,'marie','chris','tours','marie@gmail.com','0683294323','$2y$10$N.o59wuCXeDhY.G.L4nL3OZCuMUHstUrbPO.AHlpojDPDRSeCQqPm','guyjr.img',NULL,NULL,1,'2023-01-08 19:54:07','2023-01-08 19:54:07'),(6,'barclay','feusso','bafoussam','feusso@gmail.com','','$2y$10$N4IWoXcS7O2oYGwp7DuaEe9uFMWv2sLpAaGgS3OhA7Oal8bAWpH8.','image.png',NULL,NULL,1,'2023-01-08 20:39:03','2023-01-08 20:39:03'),(11,'helena','hen','tours','hen@gmail.com','0622294323','$2y$10$NlfETUJJ3uTZnp1TWZnQgeTdYbnKkqLqhbP3l6mn10poJGjd.bmT2','guyjr.img',NULL,NULL,1,'2023-01-08 21:54:24','2023-01-08 21:54:24'),(34,'dfadfads','fadsffa','fdasdfa','guydsafd@gmail.com','45245234','$2y$10$I8Z7J2BPHxmG2cSbJyQbq.DSrQu.2KVXGnqcqsGlCFw69/UnPYOcq','image.png',NULL,NULL,1,'2023-01-08 22:24:18','2023-01-08 22:24:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-09  0:10:49
